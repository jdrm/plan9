# 9front

Ficheros útiles, scripts, etc

## Trucos post instalación

Hacer capturas de pantalla

```
term% cat /dev/screen | topng > captura.png
```

Mostrar una lista de ventanas abiertas

```
term% winwatch
```

Para configurar las ventanas que se abren al acceder a nuestra sesión de usuario. Editamos el fichero *$home/bin/rc/riostart*

### Mantener el sistema actualizado

Actualizamos el sistema de forma rápida mediante:

```
term% sysupdate
term% cd /sys/src
term% mk install
```

## Configuración

Cambiar el teclado al español:

```
term% cat /sys/lib/kbmap/es > /dev/kbmap
```

## Comandos útiles

Obtener el historial del terminal en el que estamos:

```
grep 'term% ' /dev/text
```


## Aplicaciones

* [play](http://man.9front.org/1/play) para escuchar música
* [vdir](https:github.com/telephil9/vidr) un navegador gráfico por el sistema de archivos